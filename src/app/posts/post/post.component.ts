import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Post } from '../../shared/post.model';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  post!: Post;
  loading = false;
  postId = '';

  constructor(
    private route: ActivatedRoute,
    private http: HttpClient,
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const postId = params['id'];
      this.postId = postId;
      this.getPost(postId);
    });
  }

  getPost(id: string) {
    this.loading = true;
    this.http.get<Post>('https://rest-b290b-default-rtdb.firebaseio.com/posts/'+ id +'.json')
      .subscribe(result => {
        this.post = new Post(id, result.title, result.dateCreated, result.description);
        this.loading = false;
      });
  }

  deletePost() {
    this.loading = true;
    this.http.delete('https://rest-b290b-default-rtdb.firebaseio.com/posts/'+ this.postId +'.json').subscribe();
    setTimeout(() => {
      void this.router.navigate(['/']);
    },2000);
  }
}
