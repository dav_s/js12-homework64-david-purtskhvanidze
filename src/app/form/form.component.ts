import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Post } from '../shared/post.model';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  title = '';
  description = '';
  postId = '';
  loading = false;

  constructor(
    private http: HttpClient,
    private router: Router,
    private route: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.postId = params['id'];
    });
    if (this.postId != undefined) {
      this.http.get<Post>('https://rest-b290b-default-rtdb.firebaseio.com/posts/'+ this.postId +'.json')
        .subscribe(result => {
          this.title = result.title;
          this.description = result.description
        });
    }
  }

  addPost() {
    this.loading = true;
    const dateCreated = new Date();
    const title = this.title;
    const description = this.description;
    const body = {title, dateCreated, description};

    if (this.postId != undefined) {
      this.http.put('https://rest-b290b-default-rtdb.firebaseio.com/posts/'+ this.postId +'.json', body).subscribe();
      setTimeout(() => {
        void this.router.navigate(['/posts/' + this.postId]);
      },2000);
    } else {
      this.http.post('https://rest-b290b-default-rtdb.firebaseio.com/posts.json', body).subscribe();
      setTimeout(() => {
        void this.router.navigate(['/']);
      },2000);
    }
  }

}
