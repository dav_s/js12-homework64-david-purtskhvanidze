import { Component } from '@angular/core';

@Component({
  selector: 'app-not-found',
  template: `<div class="empty-wrap">Not found!</div>`,
})
export class NotFoundComponent {}
